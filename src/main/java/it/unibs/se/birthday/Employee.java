package it.unibs.se.birthday;

import java.time.LocalDate;

public class Employee {
    private String firstName;
    private String emailAddress;
    private LocalDate birthday;

    public Employee(String firstName, String emailAddress, LocalDate birthday) {
        this.firstName = firstName;
        this.emailAddress = emailAddress;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public LocalDate getBirthday() {
        return birthday;
    }
}
