package it.unibs.se.birthday;

import java.io.PrintWriter;

public class PrintWriterBasedGreetingsNotifier implements GreetingsNotifier {
    private PrintWriter writer;

    public PrintWriterBasedGreetingsNotifier(PrintWriter writer) {
        this.writer = writer;
    }

    @Override
    public void notifyBirthdayGreetingsTo(Employee employee) {
        writer.print(String.format("Happy birthday, %s (to be sent to %s)", employee.getFirstName(), employee.getEmailAddress()));
        writer.flush();
    }
}
