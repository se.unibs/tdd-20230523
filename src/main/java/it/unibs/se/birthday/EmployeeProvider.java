package it.unibs.se.birthday;

import java.util.Collection;
import java.util.HashSet;

public interface EmployeeProvider {
    Collection<Employee> getAllEmployees();
}
