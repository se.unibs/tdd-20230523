package it.unibs.se.birthday;

public class StringCalculator {
    private final String delimiter;

    public StringCalculator(String delimiter) {
        this.delimiter = delimiter;
    }

    public StringCalculator() {
        this(",");
    }
    public int play(String input) {
        if(input == "") {
            return 0;
        }
        var items = input.split(this.delimiter);
        int sum = 0;
        for (var item : items) {
            sum += Integer.parseInt(item);
        }
        return sum;
    }
}
