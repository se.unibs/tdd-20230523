package it.unibs.se.birthday;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;

public class FileBasedEmployeeProvider implements EmployeeProvider {
    private Path fileName;

    public FileBasedEmployeeProvider(Path fileName) {
        super();
        this.fileName = fileName;
    }

    @Override
    public Collection<Employee> getAllEmployees() {
        var result = new HashSet<Employee>();
        try {
        var rows = Files.readAllLines(fileName);
        for(var i = 0; i < rows.size(); i++) {
            if(i != 0) {
                var fields = rows.get(i).split(",");
                var dateAsText = fields[2];
                var birthday = LocalDate.parse(dateAsText.trim());
                result.add(new Employee(fields[1], fields[3].trim(), LocalDate.parse(fields[2].trim())));
            }
        }
        } catch(Exception ex) {

        }
        return result;
    }
}
