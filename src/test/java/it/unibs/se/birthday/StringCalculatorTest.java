package it.unibs.se.birthday;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class StringCalculatorTest {
    @Test
    public void restituisceZeroPerStringaVuota() {
        var input = "";
        int result = getResult(input);
        assertThat(result, equalTo(0));
    }

    @Test
    public void inputContenenteUnNumeroIntero() {
        assertThat(getResult("19"), equalTo(19));
    }

    @Test
    public void inputContenentePiùNumeriInteri() {
        assertThat(getResult("19,11,22,17"), equalTo(69));
    }

    @Test
    public void utilizzoSeparatoreCustom() {
        assertThat(new StringCalculator(";").play("19;11;22;17"), equalTo(69));
    }

    private int getResult(String input) {
        return new StringCalculator().play(input);
    }
}
