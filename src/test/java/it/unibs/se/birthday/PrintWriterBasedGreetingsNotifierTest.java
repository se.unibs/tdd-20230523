package it.unibs.se.birthday;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PrintWriterBasedGreetingsNotifierTest {
    @Test
    public void shouldNotifyInConsole() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        var writer = new PrintWriter(out);
        GreetingsNotifier notifier = new PrintWriterBasedGreetingsNotifier(writer);
        writer.flush();
        var employee = new Employee("Pietro", "pietrom@qualcosa.com", LocalDate.now());

        notifier.notifyBirthdayGreetingsTo(employee);


        var output = out.toString();
        assertThat(output, equalTo("Happy birthday, Pietro (to be sent to pietrom@qualcosa.com)"));
    }
}
