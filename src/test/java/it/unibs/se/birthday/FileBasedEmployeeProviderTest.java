package it.unibs.se.birthday;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class FileBasedEmployeeProviderTest {
    @Test
    public void shouldReturnEmptyListFromNotExistingFile() throws IOException {
        var tempFile = Path.of("not-existing");
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employees = provider.getAllEmployees();
        assertThat(employees.size(), equalTo(0));
    }

    @Test
    public void shouldReturnEmptyListFromFileContainingHeaders() throws IOException {
        var tempFile = Files.createTempFile("test-employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email"
        ));
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employees = provider.getAllEmployees();
        assertThat(employees.size(), equalTo(0));
    }

    @Test
    public void shouldReadEmployeeData() throws IOException {
        var tempFile = Files.createTempFile("test-employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@qualcosa.com"
        ));
        EmployeeProvider provider = new FileBasedEmployeeProvider(tempFile);
        var employees = provider.getAllEmployees();
        assertThat(employees.size(), equalTo(1));
        var first = employees.iterator().next();
        assertThat(first.getEmailAddress(), equalTo("pietrom@qualcosa.com"));
        assertThat(first.getBirthday(), equalTo(LocalDate.of(1978, 3, 19)));
    }
}
